package com.tipservice.tipservice.repositories;

import com.tipservice.tipservice.models.Tipservice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipserviceRepository extends JpaRepository<Tipservice, Long> {
    
}
