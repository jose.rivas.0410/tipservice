package com.tipservice.tipservice;

// import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TipserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TipserviceApplication.class, args);
		System.out.println("Hello World!");
		
		// Scanner sc = new Scanner(System.in);
		// String input;
		// do {
		// 	System.out.println("How many are in your party dear guests?: ");
		// 	int ui = sc.nextInt();
		// 	if (ui <= 6) {
		// 		System.out.println("On a scale up to 10, how was your satisfaction with us today?");
		// 		int ui2 = sc.nextInt();
		// 		switch (ui2) {
		// 			case 10:
		// 				System.out.println("Thank you kindly dear guests, your tip will be 25% of the total");
		// 				break;
		// 			case 9:
		// 				System.out.println("Thank you kindly dear guests, your tip will be 20% of the total");
		// 				break;
		// 			case 8:
		// 				System.out.println("Thank you dear guests, your tip is 15% of the total");
		// 				break;
		// 			case 7:
		// 				System.out.println("Thank you dear guests, your tip is 15% of the total");
		// 				break;
		// 			case 6:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 			case 5:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 			case 4:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 			case 3:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 			case 2:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 			case 1:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 			case 0:
		// 				System.out.println("We are sorry for the poor satisfaction of today's meal, you can choose to not tip us");
		// 				break;
		// 		}
		// 	}else if(ui > 6) {
		// 		System.out.println("We recommend the tip be 20%");
		// 	}else {
		// 		System.out.println(ui);
		// 	};
		// 	System.out.println();
		// 	System.out.println("Is there anything else that you need?: ");
		// 	input = sc.next();
		// }while(input.equalsIgnoreCase("yes"));
		// System.out.println("Thank you for coming to our restaurant");
		// sc.close();
	};

}
