package com.tipservice.tipservice.models;

// import java.util.Scanner;
import javax.persistence.*;

@Entity
@Table(name = "tipservice")
public class Tipservice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long tipNum;

	@Column(nullable = false)
	private int bill;

	@Column(nullable = false)
	private double tip25;

	@Column(nullable = false)
	private double tip20;

	@Column(nullable = false)
	private double tip15;

	@Column(nullable = false)
	private int guestNum;

	@Column(nullable = false)
	private int satisfactionNum;

	public Tipservice() {
	}

	public Tipservice(long tipNum, int bill, double tip25, double tip20, double tip15, int guestNum, int satisfactionNum) {
		this.tipNum = tipNum;
		this.bill = bill;
		this.tip25 = tip25;
		this.tip20 = tip20;
		this.tip15 = tip15;
		this.guestNum = guestNum;
		this.satisfactionNum = satisfactionNum;
	}

	public long getTipNum() {
		return tipNum;
	}

	public void setTipNum(long tipNum) {
		this.tipNum = tipNum;
	}

	public int getBill() {
		return bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	public double getTip25() {
		return tip25;
	}

	public void setTip25(double tip25) {
		this.tip25 = tip25;
	}

	public double getTip20() {
		return tip20;
	}

	public void setTip20(double tip20) {
		this.tip20 = tip20;
	}

	public double getTip15() {
		return tip15;
	}

	public void setTip15(double tip15) {
		this.tip15 = tip15;
	}

	public int getGuestNum() {
		return guestNum;
	}

	public void setGuestNum(int guestNum) {
		this.guestNum = guestNum;
	}

	public int getSatisfactionNum() {
		return satisfactionNum;
	}

	public void setSatisfactionNum(int satisfactionNum) {
		this.satisfactionNum = satisfactionNum;
	}


    // public static void main(String[] args) {
	// 	Scanner sc = new Scanner(System.in);
	// 	int bill = 180;
	// 	double tip25 = .25;
	// 	double tip20 = .20;
	// 	double tip15 = .15;
	// 	String input;
	// 	do {
	// 		System.out.println("How many are in your party dear guests?: ");
	// 		int ui = sc.nextInt();
	// 		if (ui <= 6) {
	// 			System.out.println("On a scale of 1 to 10, how was your satisfaction with us today?");
	// 			int ui2 = sc.nextInt();
	// 			switch (ui2) {
	// 				case 10:
	// 					System.out.format("Thank you kindly dear guests, your total for today is $" + (bill + (bill * tip25)));
	// 					break;
	// 				case 9:
	// 					System.out.format("Thank you kindly dear guests, your total is $" + (bill + (bill * tip25)));
	// 					break;
	// 				case 8:
	// 					System.out.format("Thank you dear guests, your total is $$" + (bill + (bill * tip20)));
	// 					break;
	// 				case 7:
	// 					System.out.format("Thank you dear guests, your total is $" + (bill + (bill * tip15)));
	// 					break;
	// 				case 6:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 				case 5:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 				case 4:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 				case 3:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 				case 2:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 				case 1:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 				case 0:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + bill);
	// 					break;
	// 			}
	// 		}else if(ui > 6) {
	// 			System.out.println("On a scale of 1 to 10, how was your satisfaction with us today?");
	// 			int ui3 = sc.nextInt();
	// 			switch (ui3) {
	// 				case 10:
	// 					System.out.format("Thank you kindly dear guests, your total for today is $" + (bill + (bill * tip25)));
	// 					break;
	// 				case 9:
	// 					System.out.format("Thank you kindly dear guests, your total is $" + (bill + (bill * tip25)));
	// 					break;
	// 				case 8:
	// 					System.out.format("Thank you dear guests, your total is $$" + (bill + (bill * tip25)));
	// 					break;
	// 				case 7:
	// 					System.out.format("Thank you dear guests, your total is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 6:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 5:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 4:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 3:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 2:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 1:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 				case 0:
	// 					System.out.format("We are sorry for the poor satisfaction of today's meal, your bill is $" + (bill + (bill * tip20)));
	// 					break;
	// 			}
	// 		}else {
	// 			System.out.println(ui);
	// 		};
	// 		System.out.println();
	// 		System.out.println("Is there anything else that you need?: ");
	// 		input = sc.next();
	// 	}while(input.equalsIgnoreCase("yes"));
	// 	System.out.println("Thank you for coming, have a wonderful day");
	// 	sc.close();
    // }
    
}
