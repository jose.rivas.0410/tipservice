package com.tipservice.tipservice.controllers;

import java.util.List;

import com.tipservice.tipservice.models.Tipservice;
import com.tipservice.tipservice.repositories.TipserviceRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TipserviceController {
    private final TipserviceRepository tipserviceRepo;

    public TipserviceController(TipserviceRepository tipserviceRepo) {
        this.tipserviceRepo = tipserviceRepo;
    }

    @GetMapping("/tipservice")
    public String index(Model model) {
        List<Tipservice> tipserviceList = tipserviceRepo.findAll();
        model.addAttribute("noTipServiceFound", tipserviceList.size() == 0);
        model.addAttribute("tipservice", tipserviceList);
        return "books/index";
    }

}
