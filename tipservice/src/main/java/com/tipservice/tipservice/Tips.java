package com.tipservice.tipservice;

import java.util.Scanner;

public class Tips {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TipsClass tips = new TipsClass(sc);

        System.out.println("Hello dear guests, please wait a moment");
        System.out.println("Please enter your bills amount");
        System.out.println(tips.bill());
        System.out.println("How many guests were you attending with today?");
        System.out.println(tips.numGuests(1, 12));
        System.out.println("Thank you very much, have a nice day");
    }
}
