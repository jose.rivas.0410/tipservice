package com.tipservice.tipservice;

import java.util.Scanner;

public class TipsClass {
    private Scanner sc = new Scanner(System.in);
    public int bill;
    public double tip25 = .25;
    public double tip20 = .20;
    public double tip15 = .15;

    public int bill() {
        this.bill = sc.nextInt();
        return bill;
    }

    public TipsClass(Scanner sc) {
        this.sc = sc;
    }

    public int billTotalTip25() {
        double billTotal1 = bill + (bill * tip25);
        return (int) billTotal1;
    }

    public int billTotalTip20() {
        double billTotal2 = bill + (bill * tip20);
        return (int) billTotal2;
    }

    public int billTotalTip15() {
        double billTotal3 = bill + (bill * tip15);
        return (int) billTotal3;
    }

    public void satisChoice(int min, int max) {
        int choice = sc.nextInt();
        if (choice >= 9) {
            System.out.format("Your bill's total is $" + billTotalTip25());
        }else if (choice == 8) {
            System.out.format("Your bill's total is $" + billTotalTip20());
        }else if(choice == 7) {
            System.out.format("Your bill's total is $" + billTotalTip15());
        }else if (choice <=6) {
            System.out.format("Your bill's total is $" + bill);
        }else {
            System.out.format("Please clarify on how your dining experience was");
        }
    }

    public void satisChoice2(int min, int max) {
        int choice = sc.nextInt();
        if (choice >= 9) {
            System.out.format("Your bill's total is $" + billTotalTip25());
        }else if (choice == 8) {
            System.out.format("Your bill's total is $" + billTotalTip20());
        }else if(choice == 7) {
            System.out.format("Your bill's total is $" + billTotalTip15());
        }else if (choice <=6) {
            System.out.format("Your bill's total is $" + bill);
        }else {
            System.out.format("Please clarify on how your dining experience was");
        }
    }

    public String numGuests(int min, int max) {
        int numGuests = sc.nextInt();
        if (numGuests <= 6) {
            System.out.println("On a scale of 1-10, how was today's meal?");
            satisChoice(1, 10);
        }else if (numGuests >= 7) {
            System.out.println("On a scale of 1-10, how was today's meal?");
            satisChoice2(1, 10);
        }else {
            System.out.println("Please clarify the amount of guests you were with");
        }
        return this.sc.nextLine();
    }

}
